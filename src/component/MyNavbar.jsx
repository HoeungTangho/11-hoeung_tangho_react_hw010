import React from 'react'
import { Container, Nav, Navbar, Button, Form, FormControl } from 'react-bootstrap'
import {Link} from 'react-router-dom'
export default function MyNavbar() {
    return (
        <div className="container-fluid">
        <Navbar bg="light" expand="lg">
        <Navbar.Brand href="home">React-Bootstrap</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
            <Nav.Link as={Link} to="/home">Home</Nav.Link>
            <Nav.Link as={Link} to="/category">Category</Nav.Link>
            <Nav.Link as={Link} to="/author">Author</Nav.Link>
            <Nav.Link as={Link} to="/post">Post</Nav.Link>
            <Nav.Link as={Link} to="/user">User</Nav.Link>
            
            </Nav>
            <Form inline>
            <FormControl type="text" placeholder="Search" className="mr-sm-2" />
            <Button variant="outline-success">Search</Button>
            </Form>
        </Navbar.Collapse>
        </Navbar>
    </div>
    )
}
