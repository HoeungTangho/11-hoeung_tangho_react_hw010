import React, { useEffect, useState } from "react";
import { Button, Col, Container, Form, Row, Table } from "react-bootstrap";
import { getAuthor } from "../../Services/MyService";

export default function Author() {
  const [author, setAuthor] = useState([]);
  useEffect(async() => {
    var result = await getAuthor()
    setAuthor(result);
  }, []);
  
  return (
    <div>
      <Container>
        <h1>Author</h1>
        <Row>
          <Col sm={8}>
            <Form>
              <Form.Group className="mb-3" controlId="formBasicEmail">
                <Form.Label>Author Name</Form.Label>
                <Form.Control type="email" placeholder="Enter email" />
              </Form.Group>

              <Form.Group className="mb-3" controlId="formBasicPassword">
                <Form.Label>Email</Form.Label>
                <Form.Control type="password" placeholder="Password" />
              </Form.Group>

              <Button variant="primary" type="submit">
                Add
              </Button>
            </Form>
          </Col>

          <Col sm={4}>
            <img
              src="https://designshack.net/wp-content/uploads/placeholder-image.png"
              className="w-100"
            ></img>
            <p>Choose Image:</p>
            <input type="file" name="chhose" id="chhose-image" />
          </Col>
        </Row>
        <br />
        <Row>
          <Col>
            <Table striped bordered hover>
              <thead>
                <tr>
                  <th>#</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Image</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {author.map((item, index) => (
                  <tr>
                    <td>{item._id}</td>
                    <td>{item.name}</td>
                    <td>{item.email}</td>
                    <td><img src={item.image} alt="" style={{width:'100px'}} /></td>
                    <td>
                      <Button variant="warning">Edit</Button>{" "}
                      <Button variant="danger">Delete</Button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </Col>
        </Row>
      </Container>
    </div>
  );
}
