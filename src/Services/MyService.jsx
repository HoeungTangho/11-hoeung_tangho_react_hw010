import axios from 'axios'

let api = axios.create({baseURL:"http://110.74.194.124:3034/api"}) 

    
export const getAuthor = async() => {
    var result = await api.get("/author")
  
    return result.data.data
  }