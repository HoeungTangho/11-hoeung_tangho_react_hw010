import React from 'react'
import MyNavbar from './component/MyNavbar'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Author from './component/Author/Author'
import Home from './component/Home/Home'
import Category from './component/Category/Category'
import Post from './component/Post/Post'
import User from './component/User/User'

export default function App() {
  return (
    <div>
      <Router>
      <MyNavbar/>
          <Switch>
            <Route>
            <Route exact path='/' render={ ()=><div><Home/></div>} />
            <Route  path="/author" component={Author} />
            <Route  path='/category' component={Category} />
            <Route  path='/home' component={Home} />
            <Route  path='/post' component={Post}/>
            <Route  path='/user' component={User}/>
            </Route>
          </Switch>
        
      </Router>
    </div>
  )
}
